#!/bin/bash

# Function to install Java
install_java() {
    echo "Installing Java..."
    apt-get update
    apt-get install -y openjdk-11-jdk
}

# Function to install Maven
install_maven() {
    echo "Installing Maven..."
    apt-get update
    apt-get install -y maven
}

# Function to install Nginx
install_nginx() {
    echo "Installing Nginx..."
    apt-get update
    apt-get install -y nginx
}

# Check and install Java if not installed
if ! command -v java &> /dev/null; then
    install_java
fi

# Check and install Maven if not installed
if ! command -v mvn &> /dev/null; then
    install_maven
fi

# Check and install Nginx if not installed
if ! command -v nginx &> /dev/null; then
    install_nginx
fi

echo "Before Install script completed successfully."
