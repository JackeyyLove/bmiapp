#!/bin/bash
cd /var/www/html

# Run Maven clean package to create the JAR file
mvn clean package
cp nginx_config /etc/nginx/sites-available/bmi
# Check if the default file exists before removing it
if [ -f /etc/nginx/sites-enabled/default ]; then
    rm /etc/nginx/sites-enabled/default
    echo "Removed default file"
else
    echo "No default file found, skipping removal"
fi
if [ -f /etc/nginx/sites-available/default ]; then
    rm /etc/nginx/sites-available/default
    echo "Removed default file"
else
    echo "No default file found, skipping removal"
fi
# Check if the symbolic link to "bmi" exists before creating it
if [ ! -L /etc/nginx/sites-enabled/bmi ]; then
    ln -s /etc/nginx/sites-available/bmi /etc/nginx/sites-enabled/bmi
    echo "Created symbolic link to bmi"
else
    echo "Symbolic link to bmi already exists, skipping creation"
fi
apt-get update
apt-get install dos2unix
find / -type f -name 'application_start.sh' -exec dos2unix {} \;
sed -i -e 's/\r$//' scripts/application_start.sh
chmod +x scripts/application_start.sh

sudo nginx -t
sudo nginx -s reload
# Start Spring Boot app
nohup java -jar target/bmi-1.0.jar &