<<<<<<< HEAD
FROM openjdk:11-jre-slim

# Set working directory
WORKDIR /app

# Copy the Spring Boot JAR file into the Docker image
COPY target/bmi-1.0.jar /app/bmi-1.0.jar

# Copy Nginx configuration file into the Docker image
COPY nginx_config /etc/nginx/sites-available/bmiapp

# Install Nginx
RUN apt-get update && \
    apt-get install -y nginx && \
    rm -rf /var/lib/apt/lists/*

# Remove default Nginx configuration
RUN rm /etc/nginx/sites-enabled/default

# Link Nginx configuration
RUN ln -s /etc/nginx/sites-available/bmiapp /etc/nginx/sites-enabled/bmiapp

# Expose ports
EXPOSE 80

# Command to start Nginx and your Spring Boot application
CMD service nginx start && java -jar bmi-1.0.jar
=======
# Use a base image with OpenJDK installed
FROM openjdk:11-jre-slim

# Set the working directory inside the container
WORKDIR /app

# Copy the packaged Spring Boot application JAR file into the container
COPY target/bmi-1.0.jar /app/bmi-1.0.jar

# Expose the port that the Spring Boot application will run on
EXPOSE 8000

# Command to run the Spring Boot application when the container starts
CMD ["java", "-jar", "bmi-1.0.jar"]
>>>>>>> 05950e9158f33810b67d55a41b51fdeeff360ff5
