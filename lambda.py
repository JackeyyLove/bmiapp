import boto3
def lambda_handler(event, context):
    # Extract necessary details from the event
    event_detail = event['detail']
    autoscaling_group_name = event_detail['AutoScalingGroupName']
    instance_id = event_detail.get('EC2InstanceId')  
    detail_type = event['detail-type']

    # Initialize response variable
    response = {}

    if detail_type == "EC2 Instance Launch Successful":
        try:
            # Create CloudWatch client
            cloudwatch = boto3.client('cloudwatch')
            # Create Autoscaling client
            asg = boto3.client('autoscaling')
            mem_policy_arn = None
            # Create memory scaling policy
            scaling_policy_response = asg.put_scaling_policy(
            AutoScalingGroupName=autoscaling_group_name,
            PolicyName="Memory-scaling-policy-" + instance_id,
            PolicyType="StepScaling",
            AdjustmentType="ChangeInCapacity",
            StepAdjustments=[{
                "MetricIntervalLowerBound" : 0, 
                "ScalingAdjustment" : 1
            }],
            EstimatedInstanceWarmup=300
            )
            mem_policy_arn = scaling_policy_response.get('PolicyARN')
            # Create alarm
            cloudwatch.put_metric_alarm(
                AlarmName='Memory-alarm-' + instance_id,
                ComparisonOperator='GreaterThanOrEqualToThreshold',
                EvaluationPeriods=1,
                MetricName='mem_used_percent',
                Namespace='CWAgent',
                Period=60,
                Statistic='Average',
                Threshold=40.0,
                AlarmActions=[mem_policy_arn],
                AlarmDescription='Alarm when server memory usage exceeds 40%',
                Dimensions=[
                    {
                        'Name': 'InstanceId',
                        'Value': instance_id
                    }
                ],
                Unit='Percent'
            )
            # If successful, set response statusCode and body
            response = {
                'statusCode': 200,
                'body': 'SUCCESS'
            }
        except Exception as e:
            response = {
                'statusCode': 500,
                'body': f'ERROR: {str(e)}'
            }

    elif detail_type == "EC2 Instance Terminate Successful":
        try:
            cloudwatch = boto3.client('cloudwatch')
            # Delete alarm
            cloudwatch.delete_alarms(
                AlarmNames=['Memory-alarm-' + instance_id],
            )
            # If successful, set response statusCode and body
            response = {
                'statusCode': 200,
                'body': 'SUCCESS'
            }
        except Exception as e:
            # If there's an error, log it and set response statusCode and body accordingly
            response = {
                'statusCode': 500,
                'body': f'ERROR: {str(e)}'
            }
    return response